import csv
from tkinter import Tk, Entry, StringVar, Label, IntVar
from infi.systray import SysTrayIcon
from tkinter import ttk
import sv_ttk
import webbrowser
from urllib.parse import quote

root = None

def read_csv(filename):
    data = []
    with open(filename, 'r') as f:
        lines = f.readlines()
        for line in lines:
            row = line.strip().split(',')
            data.append(row)
    return data

def show_textbox(systray):
    global root
    if root is None:
        def return_entry(en):
            print("User input: " + en.get())
            root.withdraw()  # Hide the window

        def search():
            selected_count = 0
            first_url = True
            for i in range(len(data)):
                for j in range(len(data[i])):
                    if len(data[i][j]) > 2 and data[i][j][2].get() == 1:
                        selected_count += 1
                        if data[i][j][1]:
                            url = data[i][j][1].replace('SEARCH_PHRASE', quote(search_text.get()))
                            if first_url:
                                webbrowser.open_new(url)
                                first_url = False
                            else:
                                webbrowser.open_new_tab(url)
            if selected_count > total_checkboxes / 2:
                search_button.config(text="Blast Away")
            else:
                search_button.config(text="Search")
            #print(data)
            #print(search_text.get())

        def update_button_text():
            selected_count = sum(cell[2].get() for row in data for cell in row if isinstance(cell, list) and len(cell) > 2)
            if selected_count > total_checkboxes / 2:
                search_button.config(text="Blast Away")
            else:
                search_button.config(text="Search")        
            #print(selected_count)
            
        def reset_widgets():
            # Reset the state of the widgets
            for i in range(len(data)):
                for j in range(len(data[i])):
                    if len(data[i][j]) > 2:
                        data[i][j][2].set(0)
            toggle_all_var.set(0)
            search_text.set('')
            sv_ttk.set_theme("light")

        def toggle_all():
            for i in range(len(data)):
                for j in range(len(data[i])):
                    if len(data[i][j]) > 2:
                        data[i][j][2].set(toggle_all_var.get())

        def hide_window():
            root.withdraw()
            
        root = Tk()
        root.protocol('WM_DELETE_WINDOW', hide_window)  # Hide the window when 'X' is clicked
        root.title("Universal Search")
        search_text = StringVar()

        # Set the theme to "winnative"
        sv_ttk.set_theme("light")

        data = read_csv('search_phrase.csv')
        total_checkboxes = 0
        for i in range(len(data)):
            for j in range(len(data[i])):
                parts = data[i][j].split('::')
                data[i][j] = [parts[0], False if len(parts) == 1 else parts[1], IntVar(value=0)]
                if len(parts) > 1:
                    ttk.Checkbutton(root, text=parts[0], variable=data[i][j][2], command=update_button_text).grid(row=i+1, column=j, sticky='w')
                    total_checkboxes += 1
                else:
                    Label(root, text=parts[0], font=('bold')).grid(row=i+1, column=j)

        Entry(root, textvariable=search_text, bd=5).grid(row=0, column=0, columnspan=len(data[0]), sticky="ew")
        search_button = ttk.Button(root, text="Search", command=search)
        search_button.grid(row=0, column=len(data[0])+1)
        toggle_all_var = IntVar(value=0)
        ttk.Checkbutton(root, text="Toggle All", variable=toggle_all_var, command=lambda: [toggle_all(), update_button_text()]).grid(row=len(data)+1, column=0, columnspan=len(data[0]))
        root.grid_columnconfigure(0, weight=1)
        root.mainloop()
    else:
        root.deiconify()
    
def on_quit_callback(systray):
    root.destroy()  # Destroy the window when the system tray icon is right-clicked and 'Quit' is selected

menu_options = (("Show Textbox", None, show_textbox),)
systray = SysTrayIcon("search.ico", "Universal Search", menu_options, on_quit=on_quit_callback)
systray.start()
