import time
import re
from pynput.keyboard import Key, Controller
from tkinter import Tk, Button, Entry, Label, StringVar, filedialog

keyboard = Controller()

def open_file():
    file_path = filedialog.askopenfilename(filetypes=[('Text Files', '*.txt')])
    with open(file_path, 'r') as file:
        return file.read()

def start_typing():
    text = open_file()
    speed = int(speed_entry.get())
    lines = text.split('\n')
    for line in lines:
        flag = False
        for char in line:
            if char == '[':
                flag = True
                sec = re.findall(r'\d', line)
                time.sleep(int(sec[0]))
            else:
                if flag:
                    flag = False
                else:
                    keyboard.type(char)
                    time.sleep(60/speed)
        keyboard.press(Key.enter)
        keyboard.release(Key.enter)

root = Tk()
root.title("Auto Typer")

speed_label = Label(root, text="Speed (characters per minute):")
speed_label.pack()

speed_entry = Entry(root)
speed_entry.pack()

start_button = Button(root, text="Start Typing", command=start_typing)
start_button.pack()

root.mainloop()
