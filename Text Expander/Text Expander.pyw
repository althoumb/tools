import csv
import tkinter as tk
from tkinter import simpledialog, ttk
from pynput.keyboard import Key, Listener, Controller
from pathlib import Path
import threading

# Global vars
shortcuts = {}
shortcut_buffer = ''
keyboard_controller = Controller()
CSV_PATH = Path(__file__).parent / "shortcuts.csv"

# Globals to monitor the state of caps_lock and shift
caps_lock_active = False
shift_active = False

# Load shortcuts from CSV
def load_shortcuts():
    if CSV_PATH.exists():
        if is_empty_csv(CSV_PATH):
            return {}
        else:
            with open(CSV_PATH, 'r') as file:
                reader = csv.reader(file)
                return {rows[0]: rows[1] for rows in reader}
    return {}

def is_empty_csv(path):
    with open(path) as csvfile:
        reader = csv.reader(csvfile)
        for rows in reader:
            return False
    return True

# Save shortcuts to CSV
def save_shortcuts():
    with open(CSV_PATH, 'w', newline='') as file:
        writer = csv.writer(file)
        for key, value in shortcuts.items():
            writer.writerow([key, value])

# Add a new shortcut
def add_shortcut():
    shortcut = simpledialog.askstring("Shortcut", "Enter the shortcut (e.g., /ty):")
    expanded_text = simpledialog.askstring("Expanded Text", "Enter the expanded text (e.g., thank you):")
    
    if shortcut and expanded_text:
        shortcuts[shortcut] = expanded_text
        save_shortcuts()
        update_treeview()

# Toggle the visibility of the treeview
def toggle_treeview():
    if treeview.winfo_ismapped():
        treeview.grid_forget()
    else:
        update_treeview()
        treeview.grid(row=3, column=0, columnspan=2, pady=20)

# Update the content of the treeview
def update_treeview():
    treeview.delete(*treeview.get_children())
    for shortcut, text in shortcuts.items():
        treeview.insert("", tk.END, values=(shortcut, text))

# Keyboard listener
def on_key_release(key):
    global shortcut_buffer, shift_active, caps_lock_active

    if key == Key.shift:
        shift_active = False
    elif key == Key.caps_lock:
        caps_lock_active = not caps_lock_active

    try:
        char = key.char
        shortcut_buffer += char
    except AttributeError:
        pass

    for shortcut, text in shortcuts.items():
        if shortcut_buffer.endswith(shortcut):
            # Delete the shortcut sequence
            for _ in range(len(shortcut)):
                keyboard_controller.press(Key.backspace)
                keyboard_controller.release(Key.backspace)

            # Convert special escape sequences to actual actions
            segments = text.split("\\n")
            for i, segment in enumerate(segments):
                # Handle each character in the segment
                for char in segment:
                    if shift_active:
                        keyboard_controller.press(Key.caps_lock)
                        keyboard_controller.release(Key.caps_lock)                        
                        keyboard_controller.type(char)
                        keyboard_controller.press(Key.caps_lock)
                        keyboard_controller.release(Key.caps_lock)
                    else:
                        keyboard_controller.type(char)
                
                if i < len(segments) - 1:  # If it's not the last segment
                    keyboard_controller.press(Key.shift)
                    keyboard_controller.press(Key.enter)
                    keyboard_controller.release(Key.enter)
                    keyboard_controller.release(Key.shift)

            shortcut_buffer = ''

def on_key_press(key):
    global shift_active

    if key == Key.shift:
        shift_active = True

def keyboard_listener():
    with Listener(on_press=on_key_press, on_release=on_key_release) as listener:
        listener.join()

# Start a separate thread for the keyboard listener
thread = threading.Thread(target=keyboard_listener)
thread.start()

# GUI window
root = tk.Tk()
root.title("Text Expander")

frame = tk.Frame(root, padx=20, pady=20)
frame.pack(padx=10, pady=10)

title = tk.Label(frame, text="Text Expander", font=("Arial", 20))
title.grid(row=0, column=0, columnspan=2)

add_button = tk.Button(frame, text="Add Shortcut", command=add_shortcut)
add_button.grid(row=1, column=0, pady=20)

toggle_button = tk.Button(frame, text="Toggle List", command=toggle_treeview)
toggle_button.grid(row=1, column=1, pady=20)# Treeview to display shortcuts
treeview = ttk.Treeview(frame, columns=("Shortcut", "Expanded Text"))
treeview.heading("Shortcut", text="Shortcut")
treeview.heading("Expanded Text", text="Expanded Text")
treeview.grid(row=3, column=0, columnspan=2, pady=20)

# Load shortcuts
shortcuts = load_shortcuts()
update_treeview()

root.mainloop()

# Stop the keyboard listener when the GUI is closed
thread.join()
