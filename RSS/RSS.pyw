import feedparser
import tkinter as tk
from infi.systray import SysTrayIcon
from threading import Thread, Lock, Condition
import webbrowser
from tkinter import font as tkFont
from queue import Queue
import random
from bs4 import BeautifulSoup

# List of RSS feed URLs
rss_feeds = [
    "https://www.reutersagency.com/feed/?taxonomy=best-sectors&post_type=best",
    "https://rsshub.app/apnews/topics/ap-top-news",
    "https://www.ft.com/world?format=rss",
    "https://feeds.bloomberg.com/markets/news.rss",
    "https://feeds.bloomberg.com/politics/news.rss",
    "https://feeds.bloomberg.com/technology/news.rss",
    "https://feeds.bloomberg.com/wealth/news.rss"
]

# Initialize the fetch queue with RSS feed URLs
#random.shuffle(rss_feeds)
fetch_queue = Queue()
for url in rss_feeds:
    fetch_queue.put(url)

# Function to fetch and parse RSS feeds
def fetch_headlines():
    headlines = []
    for url in rss_feeds:
        feed = feedparser.parse(url)
        for entry in feed.entries:
            headlines.append((entry.title, entry.link, entry.get("description", "")))
    return headlines

# Function to create and show the headlines window
def show_headlines(systray):
    root = tk.Tk()
    root.title("RSS Headlines")
    root.geometry("800x600")
    root.configure(background='white')
    times_font = tkFont.Font(family="Times New Roman", size=12)
    headlines_per_page = 15
    buffer_size = headlines_per_page * (5 + 1)  # Current page + 5 pages
    current_page = 0
    headlines = []
    headlines_lock = Lock()
    headlines_condition = Condition(headlines_lock)

    # Function to fetch and parse RSS feeds
    def fetch_headlines():
        for url in rss_feeds:
            feed = feedparser.parse(url)
            with headlines_condition:
                for entry in feed.entries:
                    headlines.append((entry.title, entry.link, entry.get("description", "")))
                    if len(headlines) >= headlines_per_page:
                        headlines_condition.notify()  # Signal that the first page is ready
                    if len(headlines) >= buffer_size:
                        return

    # Start fetching headlines in the background
    fetch_thread = Thread(target=fetch_headlines)
    fetch_thread.start()

    # Wait for the first page of headlines to be ready
    with headlines_condition:
        while len(headlines) < headlines_per_page:
            headlines_condition.wait()

    def fetch_more_headlines():
        with headlines_lock:
            buffer_size = headlines_per_page * (5 + current_page + 1)
            # Calculate how many headlines we have left beyond the current page
            headlines_remaining = len(headlines) - (current_page + 1) * headlines_per_page
            # If we have fewer than 5 pages of headlines left, fetch more
            if headlines_remaining < headlines_per_page * 5:
                # Fetch headlines from the next feed URL in the queue
                while not fetch_queue.empty() and len(headlines) < buffer_size:
                    url = fetch_queue.get()
                    feed = feedparser.parse(url)
                    for entry in feed.entries:
                        headlines.append((entry.title, entry.link, entry.get("description", "")))
                        if len(headlines) >= buffer_size:
                            break            
            next_button.config(state='normal')

    def update_headlines(page):
        for widget in frame.winfo_children():
            widget.destroy()
        start = page * headlines_per_page
        end = start + headlines_per_page
        for headline, link, description in headlines[start:end]:
            label = tk.Label(frame, text=headline, font=times_font, bg='white', anchor='w')
            label.pack(fill='x')
            label.bind("<Button-1>", lambda e, url=link: webbrowser.open(url))
            create_tooltip(label, description)
            separator = tk.Frame(frame, height=2, bd=1, relief='sunken', bg='black')
            separator.pack(fill='x', padx=5, pady=5)
        # Re-enable the "next" button if we have more headlines to show
        if (page + 1) * headlines_per_page < len(headlines):
            next_button.config(state='normal')

    def create_tooltip(widget, text):
        if not text:  # Check if the text is empty or None
            return  # Do not create a tooltip for empty or None text
            
        soup = BeautifulSoup(text)
        text = soup.get_text()

        tooltip = tk.Toplevel(widget, bg="lightyellow", padx=3, pady=3)
        tooltip.wm_overrideredirect(True)
        tooltip.wm_geometry("+0+0")
        tooltip.withdraw()
        # Set wraplength to an estimated pixel width for 40 words
        # You may need to adjust this value based on your font and desired width
        wraplength = 300  # Example pixel width; adjust as needed
        label = tk.Label(tooltip, text=text, bg="lightyellow", wraplength=wraplength)
        label.pack()

        def on_enter(event):
            x, y, cx, cy = widget.bbox("insert")
            x += widget.winfo_rootx() + 25
            y += widget.winfo_rooty() + 20
            tooltip.wm_geometry("+%d+%d" % (x, y))
            tooltip.deiconify()

        def on_leave(event):
            tooltip.withdraw()

        widget.bind("<Enter>", on_enter)
        widget.bind("<Leave>", on_leave)

    def next_page():
        nonlocal current_page
        current_page += 1
        update_headlines(current_page)
        prev_button.config(state='normal')
        if (current_page + 1) * headlines_per_page >= len(headlines):
            next_button.config(state='disabled')
            # Trigger fetching more headlines if we're nearing the end of the buffer
            fetch_thread = Thread(target=fetch_more_headlines)
            fetch_thread.start()
            
    def prev_page():
        nonlocal current_page
        current_page -= 1
        update_headlines(current_page)
        next_button.config(state='normal')
        if current_page == 0:
            prev_button.config(state='disabled')

    frame = tk.Frame(root, bg='white')
    frame.pack(expand=True, fill='both')

    nav_frame = tk.Frame(root, bg='white')
    nav_frame.pack(side='bottom', fill='x')

    prev_button = tk.Button(nav_frame, text='Previous', command=prev_page)
    prev_button.pack(side='left')
    next_button = tk.Button(nav_frame, text='Next', command=next_page)
    next_button.pack(side='right')

    if len(headlines) <= headlines_per_page:
        next_button.config(state='disabled')
    prev_button.config(state='disabled')

    update_headlines(current_page)
    root.mainloop()

# Function to run the headlines window in a separate thread
def on_left_click(systray):
    Thread(target=show_headlines, args=(systray,)).start()

# Function to safely exit the program
def on_exit(systray):
    tk._exit()

# Main function to set up the system tray icon
def main():
    menu_options = (("View Headlines", None, on_left_click),)
    systray = SysTrayIcon("icon.ico", "RSS Viewer", menu_options, on_quit=on_exit)
    systray.start()

if __name__ == "__main__":
    main()
